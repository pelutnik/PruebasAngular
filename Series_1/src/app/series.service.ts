import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {  HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Serie } from './serie';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';

@Injectable()
export class SeriesService {
  private baseUrl = 'http://api.tvmaze.com/shows';
  // series: Serie[];
  constructor(private http: HttpClient) { }

  getSeries() {
    //  return this.http.get<Serie[]>(this.baseUrl);
    return this.http.get<Serie[]>('http://api.tvmaze.com/shows');
  }
}
