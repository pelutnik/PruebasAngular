export class Serie {
    private id: number;
    private url: string;
    private name: string;
    private type: string;
    private languaje: string;
    private image: string;

    constructor(public _id: number, public _url: string,
                public _name: string, public _type: string,
                public _languaje: string, public _image: string) {
        this.id = _id;
        this.name = _name;
        this.url = _url;
        this.type = _type;
        this.languaje = _languaje;
        this.image = _image;
    }

    static fromJSON(data: any) {
        /*if(!data.show_title){
            throw new Error("Estructura de JSON incorrecta");
        }
        return new Serie(data.show_title, data.summary, true);*/
    }

    public toString(): string {
        return 'Id = ' + this.id + 'Name = ' + this.name;
    }
}
