import { SeriesService } from './../series.service';
import { Component, OnInit } from '@angular/core';
import { Serie } from './../serie';
@Component({
  selector: 'app-listado-series',
  templateUrl: './listado-series.component.html',
  styleUrls: ['./listado-series.component.css'],
  providers: [ SeriesService]
})
export class ListadoSeriesComponent implements OnInit {

  private series: Serie[];
  // private baseUrl = 'http://api.tvmaze.com/shows';

  constructor(private seriesService: SeriesService) { }

  ngOnInit() {
    this.seriesService.getSeries().subscribe(_series => this.series = _series );
  }
}
