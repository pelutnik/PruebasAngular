import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ListadoSeriesComponent } from './listado-series/listado-series.component';


@NgModule({
  declarations: [
    AppComponent,
    ListadoSeriesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [ListadoSeriesComponent]
})
export class AppModule { }
